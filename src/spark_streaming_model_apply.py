import json
import time

from kafka import KafkaProducer
from pyspark import SparkContext
from pyspark.mllib.clustering import KMeansModel
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

import data_format as dft

# Dft is package defined at ANA Lab for data transformation


# Function to send info to kafka



# Opens config file.


with open('project.conf') as data_file:
    config = json.load(data_file)

# Create SparkContext() and StreamingContext(), our batch size its on configStreaming.

sc = SparkContext()
ssc = StreamingContext(sc, config["streamBatchSize"])

# Kafka topic where streaming is reading and checkpoint definition.
kafkaParams = {"metadata.broker.list": config["kafkaLocation"]}

while True:
    try:
        directKafkaStream = KafkaUtils.createDirectStream(ssc, [config["dataTopic"]], kafkaParams)
        break
    except Exception as e:
        print("Error: Waiting for kafka...{}".format(e))
        time.sleep(5)

ssc.checkpoint(config["checkpointKafka"])
sameModel = KMeansModel.load(sc, config["model_path"])
# Data is received and format is given to it.

data = directKafkaStream.map(lambda x: x[1]).map(dft.str_to_list)

kpis = data.map(dft.get_dimensions) \
    .map(dft.to_float) \
    .map(dft.kpis_generation)

groups = data.map(lambda x: x[0]) \
    .map(lambda group: (group, 1)) \
    .reduceByKeyAndWindow(lambda a, b: a + b, lambda a, b: a - b, config["windowSize"], config["execTime"]) \
    .reduce(dft.to_array) \
    .map(dft.get_percentages)

# Data is tagged depending on the cluster it belongs
predictions = kpis.map(sameModel.predict)
# KPI is calculated depending on how many sessions are tagged as good
result = predictions.map(lambda tag: (tag, 1)) \
    .reduceByKeyAndWindow(lambda x, y: (x + y), lambda x, y: x - y, config["windowSize"], config["execTime"]) \
    .reduce(dft.to_array) \
    .map(dft.get_resume) \
    .union(groups) \
    .reduce(dft.to_json)


# Results are send to viz-server and kafka2rest module through Kafka.
def sendkafka(data, server, topic):
    collect = data.collect()
    if len(collect) > 0:
        print "INFO: Sent kafka. {}".format(collect)
        producer = KafkaProducer(bootstrap_servers=server)
        producer.send(topic, dft.to_byte(collect))
        producer.flush()


result.foreachRDD(lambda x: sendkafka(x, config["kafkaLocationForPGF"], config["pgfTopic"]))

ssc.start()
ssc.awaitTermination()
