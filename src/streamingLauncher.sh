#!/usr/bin/env bash

# SPARK_HOME needs to be defined as enviroment variable
# If not defined add your Spark path to ~/.bash_profile
#
# export SPARK_HOME="/path/to/spark/"

$SPARK_HOME/bin/spark-submit --master local[2] --jars $SPARK_HOME/jars/spark-streaming-kafka-assembly_2.10-1.6.0.jar spark_streaming_model_apply.py
