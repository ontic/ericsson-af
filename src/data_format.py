from __future__ import division

import ast
import json
from operator import itemgetter

from kafka import KafkaClient
from kafka import SimpleProducer


def get_dimensions(x):
    out = (x[1], x[2], x[3], x[4], x[5], x[6])
    return out


def to_float(x):
    x = list(x)
    for i in range(len(x)):
        x[i] = float(x[i])
    return x


# Generate kpis
def kpis_generation(data):
    download_bandwidth = (data[1] / (data[3] + 1)) / kpi_normalization["max_download_bw"]
    packet_loss_rate = data[2] / (data[0] + 1)
    packet_rate = data[5] / (data[0] + 1)
    latency = data[4] / kpi_normalization["max_latency"]
    if packet_loss_rate == 0.0:
        packet_loss_rate = 0.4
    if packet_rate == 0.0:
        packet_rate = 0.2
    out = (download_bandwidth, packet_loss_rate, packet_rate, latency)
    return out


def to_array(x, y):
    z = list()
    if (type(x) == tuple) & (type(y) == tuple):
        z.append(x)
        z.append(y)
    elif (type(x) == list) & (type(y) == tuple):
        z = x
        z.append(y)
    elif (type(y) == list) & (type(x) == tuple):
        z = y
        z.append(x)
    elif (type(x) == list) & (type(y) == list):
        z = x + y
    return z


def get_resume(x):
    total = 0
    aqoe = 0.0
    if type(x) is list:  # [(2, 2), (3, 1)]
        for i in x:
            total += i[1]
            aqoe += cluster_scores[str(i[0])] * i[1]
        aqoe /= total
    else:  # (3,1)
        aqoe = cluster_scores[str(x[0])]
    return aqoe


def get_percentages(x):
    total = 0.0
    gold_share = 0.0
    bronze_young_share = 0.0
    bronze_corporate_share = 0.0
    silver_corporate_share = 0.0
    silver_domestic_share = 0.0
    if type(x) == list:
        for i in x:
            total += float(str(i[1]))
        for j in x:
            if j[0] == "gold":
                gold_share = round(float(j[1] / total) * 100)
            if j[0] == "silver-corporate":
                silver_corporate_share = round(float(j[1] / total) * 100)
            if j[0] == "silver-domestic":
                silver_domestic_share = round(float(j[1] / total) * 100)
            if j[0] == "bronze-corporate":
                bronze_corporate_share = round(float(j[1] / total) * 100)
            if j[0] == "bronze-young":
                bronze_young_share = round(float(j[1] / total) * 100)

        output = {"gold": gold_share, "silver_corporate": silver_corporate_share,
                  "silver_domestic": silver_domestic_share, "bronze_corporate": bronze_corporate_share,
                  "bronze_young": bronze_young_share}
        return output
    else:
        if x[0] == "gold":
            gold_share = 100
        if x[0] == "silver-corporate":
            silver_corporate_share = 100
        if x[0] == "silver-domestic":
            silver_domestic_share = 100
        if x[0] == "bronze-corporate":
            bronze_corporate_share = 100
        if x[0] == "bronze-young":
            bronze_young_share = 100
        output = {"gold": gold_share, "silver_corporate": silver_corporate_share,
                  "silver_domestic": silver_domestic_share, "bronze_corporate": bronze_corporate_share,
                  "bronze_young": bronze_young_share}
        return output


def str_to_list(x):
    x = ast.literal_eval(x)
    x = list(x)
    return x


def to_byte(x):
    b = bytes(x)
    return b


def sendkafka(result, topic):
    kafka = KafkaClient("localhost:9092")
    producer = SimpleProducer(kafka)
    for message in result:
        print(message)
        yield producer.send_messages(topic, to_byte(message))


def to_json(x, y):
    return {"vidqoe": x, "shares": y}


def get_dimensions_static(x):
    return itemgetter(16, 22, 23, 30, 51, 113)(x)


with open("model_metadata/kpi_normalization.json") as f:
    kpi_normalization = json.load(f)

with open("model_metadata/cluster_scores.json") as f:
    cluster_scores = json.load(f)
