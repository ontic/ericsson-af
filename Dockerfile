FROM ubuntu:14.04

# Install needed packages
RUN apt-get update && apt-get install -y --fix-missing \
    openjdk-7-jre \
    checkinstall \
    libreadline-gplv2-dev \
    libncursesw5-dev \
    libssl-dev \
    libsqlite3-dev \
    tk-dev \
    libgdbm-dev \
    libc6-dev \
    libbz2-dev \
    python-pip \
    wget \
    python-dev

RUN pip install \
    kafka \
    numpy

# Download Spark
RUN wget http://d3kbcqa49mib13.cloudfront.net/spark-1.6.0-bin-hadoop2.6.tgz && \
    tar -zxvf spark-1.6.0-bin-hadoop2.6.tgz && \
    mv spark-1.6.0-bin-hadoop2.6 /spark && \
    rm spark-1.6.0-bin-hadoop2.6.tgz
ENV SPARK_HOME=/spark

# Download spark-kafka
RUN mkdir /spark/jars
RUN wget http://central.maven.org/maven2/org/apache/spark/spark-streaming-kafka-assembly_2.10/1.6.0/spark-streaming-kafka-assembly_2.10-1.6.0.jar -P /spark/jars

# Copy the code
RUN mkdir analytics-module
ADD src /analytics-module
ADD config_files/log4j.properties /spark/conf

# Add entrypoint
ADD entrypoint.sh /start-container
RUN chmod +x /start-container
ENTRYPOINT /start-container
