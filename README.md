Spark Streaming-based Analytics Function able to detect QoE degradation 
situations from the network traffic traces in Tstat format forwarded from a 
Network Trace Forwarder. 

The AF is based on Spark Streaming on Apache Spark 1.6.0. The functionality has 
been developed using Python, by means of the pyspark.mllib.clustering module 
from the pyspark.mllib package. It is able to classify Tstat traces as 
belonging to a cluster labeled as good/medium/bad. Each MOS-like category is 
assigned a numeric value and the average value of this KPI is computed during a 
time window. When the KPI value goes below a threshold, an alarm is delivered 
to the Policy Governance Function. 

When the KPI recovers for a sustained time, an end of warning alarm is sent.
The Analytics Function is fully Kafka-enabled, playing the role of a Kafka 
consumer to receive network traces from the Virtual Proxy and Kafka producer 
to deliver QoE insights to the Policy Governance Function.

The [kafka adaptor module](https://gitlab.com/ontic/ericsson-kafka-adaptor) 
is in charge of the output in order to deliver the insights to the 
[PGF](https://gitlab.com/ontic/ericsson-pgf-server).

The image is available in [Docker Hub](https://hub.docker.com/r/onticericssonspain/af/) 
and can be executed in a [Docker Compose environment](https://gitlab.com/ontic/ericsson-uc3-demo2)
